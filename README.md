# README #

Idea 1:  
- have a versioned dir to keep the contents of /opt/cenx;  
- make /opt/cenx a symlink to this dir.

The symlink should facilitate changing to a different project.

Also good to record changes to the configs:  
- some problem happens  
(such as failing to build the fault-graph because the kafka-topic names in parker and heimdallr are different);  
- I change the versioned config to fix it;  
- the repo history may help me solve similar problems in the future.

Idea 2: generate configs for projects using the devops templates and instantiate.py.
```
#!sh

17:18:33 ~/Documents/src/docker-deployer/utilities (integration)
$ ./instantiate.py -k -y ~/Documents/code/configs/mpn.yaml \
> ~/Documents/src/devops/deployments/medium/mpn-v4/ \
> ~/Documents/code/configs/mpn-v4/
```