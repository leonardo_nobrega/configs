{ ;;I think this will turn off casssandra
 :exclusions #{:policy-control :metrics-graphite :cygnus-client :shalom-client}
 :inventory-solr-url "http://192.168.99.100:8983/solr/"
 :event-service-solr-url "http://192.168.99.100:8983/solr/"
;; configs to appease poor bifrost coding
 :reconciliation-solr-url "http://192.168.99.100:8983/solr/"
 :reconciliation-solr-connection-type :http
 :keyspace-prefix "devmpnv4"
;; end appeasement configs
 :solr-zookeeper-url "192.168.99.100:2181/solr"
 :fault-url "http://192.168.99.100:8080/fault/"
 :domain-controller "http://192.168.99.100:9990/management"
 :kafka-url "192.168.99.100:9092"
 :zookeeper-url "192.168.99.100:2181"
 ;; :cassandra-urls ["127.0.0.1"]
 ;; :cassandra-keyspace "devmpnv4parameters"
 :file-upload-targets {:base-dir "/opt/cenx/application/bifrost/"
                       :upload-dir "file_uploads/"
                       :remote-user "ec2-user"
                       :remote-host "192.168.99.100"
                       :remote-dir "/cenx/sync/upload"}
 :csv-report-dir "/opt/cenx/application/bifrost/report/"
 :max-event-rows 5000
 :stream-batch-size 2000
 :solr-cloud? false
 :geo-search {:flavor :esri
              :url "http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates"}}
