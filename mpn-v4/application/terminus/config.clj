{:containers {:audit {:protocol "command"
                      :port "8815"
                      :host "192.168.99.100"
                      :node-types [:audit-build-node]
                      :graph-types [:job-graph]}}
 :client-config {:solr-url "http://192.168.99.100:8983/solr/",
                 :kafka-url "192.168.99.100:9092",
                 :zookeeper-url "192.168.99.100:2181",
                 :kafka-group-id "audit1"},
 :event-config {:ttl-in-days 30},
 :instrumentation {:metric-logging-path "/opt/cenx/terminus/metric",
                   :metric-logging-mode [:jmx]},
 :data-sources {:document-repository "vzw"
                :hippalectryon {:solr-url "http://192.168.99.100:8983/solr/"
                                :solr-zookeeper-url "192.168.99.100:2181/solr"}
                },
 :build-wrappers {:parker {:notify-build-start {:type "BatchStart"
                                                :domain "circuit_build"}
                           :notify-build-finish {:type "BatchComplete"
                                                 :domain "circuit_build"}}
                  :audit {:pause-ingest {:type "BatchStart"
                                         :domain "audit"}
                          :unpause-ingest {:type "BatchComplete"
                                           :domain "audit"}
                          :notify-build-start {:type "InventoryBuildStarted"
                                               :domain "audit"}
                          :notify-build-finish {:type "InventoryBuildCompleted"
                                                :domain "audit"}}}}
